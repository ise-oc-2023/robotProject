//copyright Heidi MEZIANE Fabrice KAYO Jason SIOCHE Patryck KOPANSKY
#include <msp430g2553.h>
#include <string.h>
#include "header.h"

#define DATA_OUT = BIT6;
#define SCK = BIT5;
#define DATA_IN = BIT7;

int main(void)
{
    // Stop watchdog timer to prevent time out reset
    WDTCTL = WDTPW | WDTHOLD;

    if ((CALBC1_1MHZ==0xFF) || (CALDCO_1MHZ==0xFF))
    {
        __bis_SR_register(LPM4_bits); // Low Power Mode #trap on Error
    }
    else
    {
        // Factory parameters
        BCSCTL1 = CALBC1_1MHZ;
        DCOCTL = CALDCO_1MHZ;
    }

    InitUART();
    init_USCI();
    init_timer();
    init_moteur();


    TXdata('>');//envoie un caractere pour montrer que l'initialisation est terminer
    __delay_cycles(200);

    __bis_SR_register(GIE); // interrupts enabled

    while (1)
    { }

}

void InitUART(void)
{
    P1SEL |= (BIT1 | BIT2);                     // P1.1 = RXD, P1.2=TXD
    P1SEL2 |= (BIT1 | BIT2);                    // P1.1 = RXD, P1.2=TXD
    UCA0CTL1 = UCSWRST;                         // SOFTWARE RESET
    UCA0CTL1 |= UCSSEL_3;                       // SMCLK (2 - 3)

    UCA0CTL0 &= ~(UCPEN | UCMSB | UCDORM);
    UCA0CTL0 &= ~(UC7BIT | UCSPB | UCMODE_3 | UCSYNC); // dta:8 stop:1 usci_mode3uartmode
    UCA0CTL1 &= ~UCSWRST;                   // **Initialize USCI state machine**

    UCA0BR0 = 104;                              // 1MHz, OSC16, 9600 (8Mhz : 52) : 8/115k
    UCA0BR1 = 0;                                // 1MHz, OSC16, 9600
    UCA0MCTL = 10;

    /* Enable USCI_A0 RX interrupt */
    IE2 |= UCA0RXIE;
}

void init_timer(void)
{
        TA0CTL = 0 |(TASSEL_2 | ID_3);
        TA0CTL |= MC_3; //comptage en mode up
        TA0CTL |= TAIE; //autorisation interruption TAIE
        TA0CCR0 = 62500; //voir texte

    // Initialize Timer_A
        TA1CTL = 0 | (TASSEL_2 | MC_1 | ID_0);
        TA1CCR0 = 1000;
        // Initialisation PWM pour moteur A
        TA1CCR1 = 0;
        // Initialisation PWM pour moteur B
        TA1CCR2 = 0;
        TA1CCTL1 |= OUTMOD_7;
        TA1CCTL2 |= OUTMOD_7;

        __enable_interrupt();
}

void init_USCI( void )
{
    __delay_cycles(250);

    UCB0CTL0 = 0;
    UCB0CTL1 = (0 + (UCSWRST *1));

    IFG2 &= ~(UCB0TXIFG | UCB0RXIFG);

    UCB0CTL0 |= ( UCMST | UCMODE_0 | UCSYNC );
    UCB0CTL0 &= ~( UCCKPH | UCCKPL | UCMSB | UC7BIT );
    UCB0CTL1 |= UCSSEL_2;

    UCB0BR0 = 0x0A;     // divide SMCLK by 10
    UCB0BR1 = 0x00;

    P1SEL  |= ( BIT5 | BIT6 | BIT7);
    P1SEL2 |= ( BIT5 | BIT6 | BIT7);

    UCB0CTL1 &= ~UCSWRST;
    UCB0CTL1 &= ~UCSWRST;
    IE2|= UCB0RXIE;
}

void init_moteur(void)
{
    P2DIR &= ~(BIT0 | BIT3);
    P2DIR |= (BIT1|BIT2|BIT4|BIT5);
    P2SEL &= ~(BIT1|BIT5);
    P2SEL2 &= ~(BIT1 | BIT5);
    P2SEL |= (BIT2|BIT4);
    P2SEL2 &= ~(BIT2|BIT4);
    P2OUT &= ~(BIT2|BIT4);
}

void TXdata( unsigned char c )
{
    while ((IFG2 & UCA0TXIFG) == 0)
    {}  // USCI_A0 TX buffer ready?
    UCA0TXBUF = c;              // TX -> RXed character
}

void Send_STR_UART(const char *msg)
{
    int i = 0;
    while (msg[i] != '\0')//on parcour le tableau de 0 jusqu'a
                          //ce que la valeurs du tableau soit 0x00
    {
        TXdata(msg[i]); //envoie de chaque caractere 1 par 1
        i++;
    }
}

void Send_char_SPI(unsigned char carac)
{
    while ((UCB0STAT & UCBUSY) == 1)
    {}
    while ((IFG2 & UCB0TXIFG) == 0)
    {}
    UCB0TXBUF = carac;
  //  envoi_msg_UART((unsigned char *)cmd1);

    while ((UCB0STAT & UCBUSY) == 1)
    {}
    UCB0TXBUF = carac;
   // envoi_msg_UART((unsigned char *)cmd1);
}

void avancer(void)
{
    TA1CCR1=1000;
    TA1CCR2=1000;

    P2OUT &= ~BIT1;
    P2OUT |= BIT5;
}

void tourner_gauche(void)
{
    TA1CCR1 = 600;
    TA1CCR2 = 600;
    P2OUT |= (BIT1|BIT5);
    __delay_cycles(200000);
}


void reculer(void)
{
    TA1CCR1 = 500;
    TA1CCR2 = 500;
    P2OUT |=BIT1;
    P2OUT &= ~BIT5;
}

void tourner_droite(void)
{
    TA1CCR1 = 600;
    TA1CCR2 = 600;
    P2OUT &= ~(BIT1|BIT5);
    __delay_cycles(200000);
}

void arret(void)
{
    TA1CCR1 = 0;
    TA1CCR2 = 0;
}

//interruptiont
#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
unsigned char cmd1[10];
    if ((IFG2 & UCA0RXIFG) == 1)
    {
    
    unsigned char c;
    c = UCA0RXBUF;
    //TXdata(c);

    switch ( c )
    {
        //////cas pour allumer la led rouge
        case 'z':
            avancer();
            Send_STR_UART("\n\rLe robot avance");
        break;

        //////cas pour allumer la led rouge
        case 's':
            reculer();
            Send_STR_UART("\n\rLe robot recule");

        break;

        //////cas pour allumer la led rouge
        case 'd':
            tourner_droite();
            Send_STR_UART("\n\rLe robot effectue une rotation sur la droite");
        break;

        //////cas pour allumer la led rouge
        case 'q':
            tourner_gauche();
            Send_STR_UART("\n\rLe robot effectue une rotation sur la gauche");
        break;
        //////cas pour allumer la led rouge
        case 'a':
            arret();
            Send_STR_UART("\n\rLe robot c'est arret�");
        break;
        case 'k':
            Send_char_SPI((unsigned char)0x31);
            Send_STR_UART("\n\rLe servomoteur est arret�");
            break;
        default:
            Send_STR_UART("\n\rCommand Unknown");
            break;
    }

    }

    else if ((IFG2 & UCB0RXIFG) == 1)
    {
        while (((UCB0STAT & UCBUSY) ==(int)1) && ((UCB0STAT & UCOE) == (int)0) == (int)1)
        {}
        while ((IFG2 & UCB0RXIFG) == 0)
        {}
        cmd1[0] = UCB0RXBUF;
        cmd1[1] = 0x00;
        if (UCB0RXBUF==0x31)
        {
            P1OUT ^=BIT6;
            arret();
        }
    }
    else
    {}
}


#pragma vector=TIMER0_A1_VECTOR //voir diaporama seance precedente
__interrupt void ma_fnc_timer(void)
    {
        //Send_char_SPI('a');
        UCB0TXBUF = 0x00;
        //envoi_msg_UART((unsigned char *)cmd1);�� // slave echo
        P1OUT ^= BIT0; //changement d�etat LED rouge
        TA0CTL &= ~TAIFG; //RAZ TAIFG
    }