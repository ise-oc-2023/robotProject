/*
 * header.h
 *
 *  Created on: 9 mai 2023
 *      Author: heidi
 */

#ifndef HEADER_H_
#define HEADER_H_

void InitUART(void);
void init_USCI(void);
void init_timer(void);
void init_moteur(void);
void TXdata( unsigned char c );
void Send_STR_UART(const char *msg);
void Send_char_SPI(unsigned char carac);
void avancer(void);
void tourner_gauche(void);
void reculer(void);
void tourner_droite(void);
void arret(void);


#endif /* HEADER_H_ */
