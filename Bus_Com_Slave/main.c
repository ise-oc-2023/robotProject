//copyright Heidi MEZIANE Fabrice KAYO Jason SIOCH Patrick KOPANSKY
#include <msp430g2231.h>
#include <intrinsics.h>
#include "header.h"

//static unsigned int t = 0;
static unsigned long delay_measurement = 0;
/* ----------------------------------------------------------------------------
 * Fonction d'initialisation de la carte TI LauchPAD
 * Entrees: -
 * Sorties: -
*/

void main( void )
{
    init_BOARD();
    init_USI();
    init_TIMER();


    // Wait for the SPI clock to be idle (low).
    USICTL0 &= ~USISWRST;

    __bis_SR_register(LPM4_bits | GIE); // general interrupts enable & Low Power Mode
    while (1)
    {
        for (TACCR1 = 0; TACCR1<2600; TACCR1+= 100)
        {
           // TACCR1 += 100;  //CCR1 PWM Duty Cycle  !min 350 max 2600 angle 190,
            //350 2350-180 degrees

            __delay_cycles(50000);
        }

        for (TACCR1 = 0; TACCR1>0; TACCR1-= 100)
        {
        //    TACCR1 -= 100;  //CCR1 PWM Duty Cycle  !min 350 max 2600 angle 190,
            //350 2350-180 degrees

            __delay_cycles(50000);
        }
    }
}


void init_BOARD( void )
{
    // Stop watchdog timer to prevent time out reset
    WDTCTL = WDTPW | WDTHOLD;

    if ((CALBC1_1MHZ==0xFF) || (CALDCO_1MHZ==0xFF))
    {
        __bis_SR_register(LPM4_bits);
    }
    else
    {
        // Factory Set.
        DCOCTL = 0;
        BCSCTL1 = CALBC1_1MHZ;
        DCOCTL = (0 | CALDCO_1MHZ);
    }

    //--------------- Secure mode
    P1SEL = 0x00; // GPIO
    P1DIR = 0x00; // IN
    P1IE = 0x00;

    // inititialisation des led warning
    P1DIR |=  BIT0;
    P1OUT &= ~BIT0;

    // GPIO initialisation
    P1SEL &= ~(BIT4); // US-SRF Trigger Line out
    P1DIR |= (BIT4);
    P1OUT &= ~BIT4;

    P1SEL |= (BIT1); // US-SRF Echo line input capture
    P1DIR |= (BIT1);

    P1IE |= BIT3; // initialisation de l'interruption
    P1IES |= BIT3; // interruption sur front descendant
    // donc appui car bouton connecte a la masse
    P1IFG &= ~(BIT3); // RAZ flag d�interruption


//////////Servomoteur PWM/////////
    P1DIR |= BIT6;
    P1SEL |= BIT6;  //selection for timer setting
    TACCR0 = 20000;  //PWM period
    TACCR1 = 0;
    TACCTL1 = OUTMOD_7;  //CCR1 selection reset-set
    TACTL = TASSEL_2|MC_1;   //SMCLK submain clock,upmode

}


/* ----------------------------------------------------------------------------
* Fonction d'initialisation du TIMER
* Entree : -
* Sorties: -
*/
void init_TIMER(void)
{
 TA0CTL = TASSEL_2 | ID_0 | MC_1 | TACLR; // SMCLK; continuous mode, 0 -->

    BCSCTL1= CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;
/////////servomoteur init period/////////
 TA0CCR0 = 20000;
 TA0CCR1 = 0;
 TA0CCTL0 = OUTMOD_7;
 //TA0CCTL0 = CM_3 | CAP | CCIS_0 | SCS | CCIE; // falling edge & raising edge,
//capture mode, capture/compare interrupt enable
 // P2.1 (echo) is connected to CCI1A (capture/compare input )
 //TA0CCTL0 &= ~CCIFG;
}

/* ----------------------------------------------------------------------------
 * Fonction d'initialisation de l'USI POUR SPI SUR UCB0
 * Entree : -
 * Sorties: -
 */
void init_USI( void )
{
    // USI Config. for SPI 3 wires Slave Op.
    // P1SEL Ref. p41,42 SLAS694J used by USIPEx
    USICTL0 |= USISWRST;
    USICTL1 = 0;

    // 3 wire, mode Clk&Ph / 14.2.3 p400
    // SDI-SDO-SCLK - LSB First - Output Enable - Transp. Latch
    USICTL0 |= (USIPE7 | USIPE6 | USIPE5 | USILSB | USIOE | USIGE );
    // Slave Mode SLAU144J 14.2.3.2 p400
    USICTL0 &= ~(USIMST);
    USICTL1 |= USIIE;
    USICTL1 &= ~(USICKPH | USII2C);

    USICKCTL = 0;           // No Clk Src in slave mode
    USICKCTL &= ~(USICKPL | USISWCLK);  // Polarity - Input ClkLow

    USICNT = 0;
    USICNT &= ~(USI16B | USIIFGCC ); // Only lower 8 bits used 14.2.3.3 p 401 slau144j
    USISRL = 0x23;  // hash, just mean ready; USISRL Vs USIR by ~USI16B set to 0
    USICNT = 0x08;
}

// --------------------------- R O U T I N E S   D ' I N T E R R U P T I O N S

/* ************************************************************************* */
/* VECTEUR INTERRUPTION USI                                                  */
/* ************************************************************************* */
#pragma vector=USI_VECTOR
__interrupt void universal_serial_interface(void)
{
    volatile unsigned char RXDta;
    while ((USICTL1 & USIIFG) == (unsigned int)0 )   // waiting char by USI counter flag
    {}
    RXDta = USISRL;

    if (RXDta == (unsigned char)0x31) //if the input buffer is 0x31 (mainly to read the buffer)
    {
        if (t==(unsigned int)0x00)
        {
            t=1;
        }
        else
        {
            t=0;//turn on LED
        }
    }

    else
    {}
    //USISRL = RXDta;
    USICNT &= ~USI16B;  // re-load counter & ignore USISRH
    USICNT = 0x08;      // 8 bits count, that re-enable USI for next transfert
}
//------------------------------------------------------------------ End ISR

// --------------------------- R O U T I N E S D ' I N T E R R U P T I O N S
/* ************************************************************************* */
/* VECTEUR INTERRUPTION TIMER0 */
/* ************************************************************************* */

#pragma vector=PORT1_VECTOR
__interrupt void detect_bouton(void)
{
    P1OUT ^= BIT0;
    USISRL = 0x31;
    P1IFG &= ~(BIT3);
}